# How to Add a New to Your Git Repo

1. Create the file within the repo main directory.
2. Run the following commands:
```
git add .
git commit -m "Make a message describing what the commit does"
git push
```
