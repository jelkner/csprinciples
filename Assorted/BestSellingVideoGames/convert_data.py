import csv

games = []
sales = []
platforms = []
publishers = []

with open('BestSellingVideoGames.csv', newline='') as csvfile:
    gamereader = csv.reader(csvfile, delimiter=',', quotechar='"')
    next(gamereader)
    for row in gamereader:
        print(row)
        games.append(row[1])
        sales.append(row[2])
        platforms.append(row[3])
        publishers.append(row[6])

jsfile = open('bsvg.js', 'w')
jsfile.write('var games = ')
jsfile.write(str(games) + '\n')
jsfile.write('var sales = ')
jsfile.write(str(sales) + '\n')
jsfile.write('var platforms = ')
jsfile.write(str(platforms) + '\n')
jsfile.write('var publishers = ')
jsfile.write(str(publishers) + '\n')
jsfile.close()
