var games = ['Minecraft', 'Grand Theft Auto V', 'Tetris (EA Mobile)', 'Wii Sports', "PlayerUnknown's Battlegrounds", 'Super Mario Bros.', 'Pokémon Red / Green / Blue / Yellow', 'Wii Fit and Wii Fit Plus', 'Tetris (Nintendo)', 'Pac-Man', 'Mario Kart Wii', 'Mario Kart 8 / Deluxe', 'Wii Sports Resort', 'Red Dead Redemption 2', 'New Super Mario Bros.', 'Terraria', 'New Super Mario Bros. Wii', 'The Elder Scrolls V: Skyrim', 'Diablo III', 'Pokémon Gold / Silver / Crystal', 'Duck Hunt', 'Wii Play', 'The Witcher 3', 'Grand Theft Auto: San Andreas', 'Call of Duty: Modern Warfare 3', 'Call of Duty: Black Ops', 'Grand Theft Auto IV', 'Pokémon Sun / Moon / Ultra Sun / Ultra Moon', 'Pokémon Diamond / Pearl / Platinum', 'Call of Duty: Black Ops II', 'Kinect Adventures!', 'FIFA 18', 'Sonic the Hedgehog', 'Nintendogs', 'Mario Kart DS', 'Call of Duty: Modern Warfare 2', 'Pokémon Ruby / Sapphire / Emerald', 'Borderlands 2', 'Super Mario World', 'Frogger', 'Lemmings', 'Grand Theft Auto: Vice City', 'The Last of Us', 'The Legend of Zelda: Breath of the Wild', 'Brain Age', 'Super Mario Bros. 3', 'Call of Duty: Ghosts', 'Super Smash Bros. Ultimate', 'Mario Kart 7', 'Super Mario Land', 'Gran Turismo 4']
var sales = ['200000000.0', '130000000.0', '100000000.0', '82900000.0', '60000000.0', '48240000.0', '47520000.0', '43800000.0', '43000000.0', '39098000.0', '37320000.0', '33220000.0', '33130000.0', '31000000.0', '30800000.0', '30300000.0', '30300000.0', '30000000.0', '30000000.0', '29490000.0', '28300000.0', '28020000.0', '28000000.0', '27500000.0', '26500000.0', '26200000.0', '25000000.0', '24950000.0', '24730000.0', '24200000.0', '24000000.0', '24000000.0', '23982960.0', '23960000.0', '23600000.0', '22700000.0', '22540000.0', '22000000.0', '20972500.0', '20000000.0', '20000000.0', '20000000.0', '20000000.0', '19080000.0', '19010000.0', '19000000.0', '19000000.0', '18840000.0', '18710000.0', '18370500.0', '17830000.0']
var platforms = ['Multi-platform', 'Multi-platform', 'Mobile', 'Wii', 'Multi-platform', 'Multi-platform', 'Multi-platform', 'Wii', 'Game Boy / NES', 'Multi-platform', 'Wii', 'Wii U / Switch', 'Wii', 'Multi-platform', 'Nintendo DS', 'Multi-platform', 'Wii', 'Multi-platform', 'Multi-platform', 'Game Boy Color', 'NES', 'Wii', 'Multi-platform', 'Multi-platform', 'Multi-platform', 'Multi-platform', 'Multi-platform', 'Nintendo 3DS', 'Nintendo DS', 'Multi-platform', 'Xbox 360', 'Multi-platform', 'Multi-platform', 'Nintendo DS', 'Nintendo DS', 'Multi-platform', 'Game Boy Advance', 'Multi-platform', 'Multi-platform', 'Multi-platform', 'Multi-platform', 'Multi-platform', 'PS3 / PS4', 'Switch / Wii U', 'Nintendo DS', 'Multi-platform', 'Multi-platform', 'Nintendo Switch', 'Nintendo 3DS', 'Multi-platform', 'PS2 / PSP']
var publishers = ['Mojang Studios', 'Rockstar Games', 'Electronic Arts', 'Nintendo', 'PUBG Corporation', 'Nintendo', 'Nintendo', 'Nintendo', 'Nintendo', 'Namco', 'Nintendo', 'Nintendo', 'Nintendo', 'Rockstar Games', 'Nintendo', 'Re-Logic / 505 Games', 'Nintendo', 'Bethesda Softworks', 'Blizzard Entertainment', 'Nintendo', 'Nintendo', 'Nintendo', 'CD Projekt', 'Rockstar Games', 'Activision', 'Activision', 'Rockstar Games', 'Nintendo / The Pokémon Company', 'Nintendo / The Pokémon Company', 'Activision', 'Xbox Game Studios', 'Electronic Arts', 'Sega', 'Nintendo', 'Nintendo', 'Activision', 'Nintendo / The Pokémon Company', '2K Games', 'Nintendo', 'Sega', 'Psygnosis', 'Rockstar Games', 'Sony Computer Entertainment', 'Nintendo', 'Nintendo', 'Nintendo', 'Activision', 'Nintendo', 'Nintendo', 'Nintendo', 'Sony Computer Entertainment']

function randomNumber(low, high) {
    return low + Math.floor(Math.random() * (high - low));
}

function getRandomGameIndeces() {
  gameIndeces = []
  while (gameIndeces.length < 6) {
    var randIndex = randomNumber(1, 51);
    if (!gameIndeces.indexOf(randIndex) > -1) {
        gameIndeces.push(randIndex);
    }
  }
  return gameIndeces;
}

console.log(getRandomGameIndeces());
