# Adding Images in Markdown

One of my AP CSP students was having trouble adding an image to her
Markdown file, so I thought I would provide an example here.

I'll start with a publically available image from Wikipedia:

![Flower Poster](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Flower_poster_2.jpg/495px-Flower_poster_2.jpg)

Next I'll try a local image copied from our text:

![Swan](swan.jpg)
