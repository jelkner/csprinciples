# Dorothy Vaughan - Human "Computer" and FORTRAN Programmer

## Morning Announcement for Wednesday, February 9th

Dorothy Jean Johnson Vaughan was an African American mathematitian who worked
as a *human computer* for NASA at the Langley Research Center in Hampton,
Virginia in the days when computers where people and not machines.

In 1961, when the first digital computers were introduced at the agency,
Dorothy Vaughan taught herself and the women she supervised the programming
language FORTRAN, used to control the new machines. This helped her and the
other African American human computers she supervised advance in their careers
at NASA, and she became NASA's first African American manager because of her
initiative.

You can find out more about the life of Dorothy Vaughan in the movie
*Hidden Figures*, in which she is one of the main characters.
