# Computer Science Herstory

Just a place to put information about the many sheros in the history of
computer science.

The past is a terrain of struggle, since the way we shape the stories we tell
and the lessons we draw from them influence our present and future actions.
