/*
This is the source code from a Code.org project located at:
https://studio.code.org/projects/applab/aqzNUis06Uiom9t48U42sCyGlUcDgMOAeNO1R9_T7nQ/
*/
// Global variables and initialization
var classNames = getColumn("ClassLists", "className");
setProperty("selectClassDropdown", "options", classNames);
var currentClass;
var currentClassList;
var pickList = [];
getClassInfo();


// Utilities
function listContains(list, item) {
  for (var i = 0; i < list.length; i++) {
    if (list[i] == item) {
      return true;
    }
  }
  return false;
}

function csvToList(csvString) {
  // return sorted list from a string with ; separated values
  if (csvString == "") {
    return [];
  }
  if (csvString.indexOf(";") == -1) {
    return [csvString];
  }
  var studentList = csvString.split(";");
  return studentList.sort();
}

function listToCSV(list) {
  if (list.length == 0) {
    return "";
  }
  if (list.length == 1) {
    return list[0];
  }
  // list has 2 or more items
  return list.join(";");
}

function findPosToAddRemove(list, str) {
  var pos = 0;
  // locate position in list to insert str
  while (str > list[pos] && pos < list.length) {
    pos++;
  }
  return pos;
}


// Data handlers
function getClassInfo() {
  classNames = getColumn("ClassLists", "className");
  if (classNames.length == 0) {
    currentClass = "";
    currentClassList = "";
  } else {
    currentClass = getText("selectClassDropdown");
    readRecords("ClassLists", {className:currentClass}, function(records) {
      currentClassList = csvToList(records[0].studentListCSVstring);
    });  
  }
}

function updateClassList(className) {
  readRecords("ClassLists", {className:className}, function(records) {
    var classToUpdate = records[0];
    classToUpdate.studentListCSVstring = listToCSV(currentClassList);
    updateRecord("ClassLists", classToUpdate, function(record, success) {
      if (success) {
        console.log("It succeeded!");
      }
    });
  });
}


// Event handlers

// homeScreen
onEvent("selectClassDropdown", "change", function() {
  setText("outputStudents", "");
  getClassInfo();
  pickList = [];
});

onEvent("pickRandomStudentButton", "click", function() {
  setProperty("outputStudents", "font-size", 16);
  if (currentClassList.length == 0) {
    setText("outputStudents", "Sorry, no students in this class :-(");
  } else {
    // if pickList becomes empty, repopulate it
    if (pickList.length == 0) {
      pickList = currentClassList.slice();
    }
    var i = randomNumber(0, pickList.length-1);
    var pickedStudent = pickList[i];
    removeItem(pickList, i);
    setText("outputStudents", pickedStudent + ", it's your turn.");
  }
});

onEvent("pairStudentsButton", "click", function() {
    setProperty("outputStudents", "font-size", 12);
  var partner1;
  var partner2;
  var message = "HERE ARE THE PAIRS:\n";
  
  if (currentClassList.length == 0) {
    setText("outputStudents", "Sorry, no students in this class :-(");
  } else {
    pickList = currentClassList.slice();
    while (pickList.length > 1) {
      var i = randomNumber(0, pickList.length-1);
      partner1 = pickList[i];
      removeItem(pickList, i);
      i = randomNumber(0, pickList.length-1);
      partner2 = pickList[i];
      removeItem(pickList, i);
      message += partner1 + " is paired with " + partner2 + "\n";
    }
    if (pickList.length == 1) {
      message += "Poor " + pickList[0] + " has to work alone";
    }
    setText("outputStudents", message);
  }
});

onEvent("editClassListButton", "click", function() {
  setText("currentClassMessage", "Editing class list: " + currentClass);
  setProperty("selectStudentDropdown", "options", currentClassList);
  setScreen("editClassListScreen");
});

onEvent("addRemoveClassButton", "click", function() {
  setProperty("deleteClassDropdown", "options", classNames);
  setScreen("addRemoveClassScreen");
});


// addRemoveClassScreen
onEvent("deleteClassButton", "click", function() {
  var classToDelete = getText("deleteClassDropdown");
  var recordId;
  readRecords("ClassLists", {className:classToDelete}, function(records) {
    recordId = records[0].id;
    deleteRecord("ClassLists", {id:recordId}, function(success) {
      if (success) {
        classNames = getColumn("ClassLists", "className");
        setProperty("deleteClassDropdown", "options", classNames);
        setText("newClassInput", classToDelete + " deleted");
      }
    });
    setTimeout(function() {
      setText("newClassInput", "");
    }, 2000);
  });
});

onEvent("addClassButton", "click", function() {
  var classToAdd = getText("newClassInput");
  if (
       classToAdd !== "" &&
       classToAdd !== "Class Added" &&
       !listContains(classNames, classToAdd)
      )
  {
    createRecord(
      "ClassLists",
      {className:classToAdd, studentListCSVstring:""},
      function(record) {
        setText("newClassInput", record.className + " added");
        classNames = getColumn("ClassLists", "className");
        setProperty("deleteClassDropdown", "options", classNames);
      });
  } else {
    setText("newClassInput", classToAdd + " not added");
  }
  setTimeout(function() {
    setText("newClassInput", "");
  }, 2000);
});

onEvent("returnToHomeScreenButton2", "click", function() {
  setProperty("selectClassDropdown", "options", classNames);
  setScreen("homeScreen");
});


// editClassListScreen
onEvent("returnToHomeScreenButton", "click", function() {
  updateClassList(currentClass);
  setScreen("homeScreen");
});

onEvent("addStudentButton", "click", function() {
  var newStudent = getText("newStudentInput");
  if (listContains(currentClassList, newStudent)) {
    setText("newStudentInput", newStudent + " already in class, NOT added");
  } else {
    // insert newStudent in proper place and update dropdown list
    insertItem(
      currentClassList,
      findPosToAddRemove(currentClassList, newStudent),
      newStudent
    );
    setProperty("selectStudentDropdown", "options", currentClassList);
    setText("newStudentInput", newStudent + " added to " + currentClass);
  }
  setTimeout(function() {
    setText("newStudentInput", "");
  }, 2000);
});

onEvent("removeStudentButton", "click", function() {
  var removedStudent = getText("selectStudentDropdown");
  removeItem(
      currentClassList,
      findPosToAddRemove(currentClassList, removedStudent)
  );
  setProperty("selectStudentDropdown", "options", currentClassList);
  setText("newStudentInput", removedStudent + " removed from " + currentClass);
  setTimeout(function() {
    setText("newStudentInput", "");
  }, 2000);
});
