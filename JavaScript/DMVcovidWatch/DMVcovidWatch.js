/*
Project: DMVcovidWatch - Display past two day's COVID data for Virginia, DC,
         and Maryland
Author: Jeffrey Elkner
Last Modified: 2021-11-01

Resources:

1. https://www.javascripttutorial.net/javascript-callback/
2. https://stackoverflow.com/questions/208016/how-to-list-the-properties-of-a-javascript-object
3. https://www.codegrepper.com/code-examples/javascript/javascript+how+to+wait+for+a+callback+function+to+be+completed
*/
var DEBUG = true;
var stateDataList = [];
var stateDeltas = {
  DC: {},
  Maryland: {},
  Virginia: {}
};

function getDaysAgo(numDaysAgo) {
  /* Generate a date string for previous day conforming to
     date format in COVID-19 Cases per US State  */
  var weekDays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  var day = new Date();
  day.setDate(day.getDate() - numDaysAgo);
  
  return weekDays[day.getDay()] + " " + (day.getMonth() + 1).toString() +
         "/" + day.getDate().toString();
}

function getStateDayData(state, day) {
  var stateDay = [state, day];
  
  readRecords("COVID-19 Cases per US State", {State:state, Date: day}, function(records) {
    if (records.length > 0) {
      appendItem(stateDay, records[0]["Total Confirmed Cases"]);
      appendItem(stateDay, records[0]["Total Deaths"]);
      appendItem(stateDataList, stateDay);
    } else {
     console.log("No records read");
    }
  });  
}

function getStateDataListData(states, days, callback) {
  for (var s = 0; s < states.length; s++) {
    for (var d = 0; d < days.length; d++) {
      if (DEBUG) {
        console.log("Calling getStateDayData(" + states[s] + ", " + days[d] + ")");
      }
      callback(states[s], days[d], getStateDayData);
    }
  }
}

function computeStateDeltas() {
  stateDeltas.DC.newCases = stateDataList[0][2] - stateDataList[1][2];
  stateDeltas.DC.newDeaths = stateDataList[0][3] - stateDataList[1][3];
}

function runTests() {
  var theStates = ["Virginia", "Maryland", "District of Columbia"];
  var theDays = [];
  
  for (var i = 1; i < 3; i++) {
    appendItem(theDays, getDaysAgo(i));
  }
  
  getStateDataListData(theStates, theDays, getStateDayData);
  console.log("Latest DMV Covid Data requested");
  
  setTimeout(function() {
      console.log("We waited a bit so now...");
      for (var j = 0; j < stateDataList.length; j++) {
        console.log(stateDataList[j]);
      }
      console.log("We have " + stateDataList.length + " items in the list.");
      computeStateDeltas();
      console.log("DC new cases: " + stateDeltas.DC.newCases);
            console.log("DC new deaths: " + stateDeltas.DC.newDeaths);
    }, 1000
  );
  console.log("We have " + stateDataList.length + " items in the list.");
}

if (DEBUG) {
  runTests();
}
