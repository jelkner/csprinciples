const TEST = true;

function runTests(tests) {
  for (let i = 0; i < tests.length; i++) {
    if (tests[i].call == tests[i].expect) {
      console.log("Passed");
    } else {
      console.log("Failed");
    }
  };
}

function processEntry(entry) {
  const multipleOf3 = entry % 3 === 0
  const multipleOf5 = entry % 5 === 0

  if (multipleOf3 && multipleOf5) {
    return 'FizzBuzz'
  }

  if (multipleOf3) {
    return 'Fizz'
  }

  if (multipleOf5) {
    return 'Buzz'
  }

  return entry
}

function fizzBuzz(input) {
  return input.map(processEntry).join(', ')
}

if (TEST) {
  let tests = [
    {
      call: fizzBuzz([1]),
      callStr: "fizzBuzz([1])",
      expect: '1'
    },
    {
      call: fizzBuzz([1, 2]),
      callStr: "fizzBuzz([1, 2])",
      expect: '1, 2'
    },
    {
      call: fizzBuzz([1, 2, 3]),
      callStr: "fizzBuzz([1, 2, 3])",
      expect: '1, 2, Buzz'
    }
  ];
  runTests(tests);
}
