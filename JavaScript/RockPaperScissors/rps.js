/*
Program: A Node CLI Rock, Paper, Scissors
Author: Jeffrey Elkner
Date: December, 2021

Resources used to begin to learn to handle async in JavaScript:

https://nodejs.dev/learn/accept-input-from-the-command-line-in-nodejs
https://attacomsian.com/blog/nodejs-read-input-from-cli
https://stackoverflow.com/questions/47998851/node-js-readline-inside-of-promises
https://stackoverflow.com/questions/43638105/how-to-get-synchronous-readline-or-simulate-it-using-async-in-nodejs
https://www.npmjs.com/package/readline-sync
*/
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const choices = "RPS";
const winScore = 3;

function ask(prompt) {
  return new Promise((resolve, reject) => {
    rl.question(prompt, input => resolve(input));
  });
}

let player1 = ['', 0];
let player2 = ['Hal', 0];

function determineWinner(p1choice, p2choice) {
  if (p1choice == p2choice) {
    return "Draw.";
  }
  if (
    (p1choice == "R" && p2choice == "S") ||
    (p1choice == "P" && p2choice == "R") ||
    (p1choice == "S" && p2choice == "P")
  ) {
    player1[1]++;
    return `${player1[0]} wins!`;
  }
  player2[1]++;
  return `${player2[0]} wins!`;
}

function makeChoice() {
  return choices[Math.floor(Math.random() * 3)];  
}

async function main() {
  player1[0] = await ask("\nWhat's your name? ");

  while (player1[1] < winScore && player2[1] < winScore) {
    let choice = await ask(`So, ${player1[0]}, what will it be, R, P, or S? `);
    let cChoice = makeChoice();
    let theResult = `${player1[0]} chose ${choice} and ${player2[0]}`;
    theResult += ` chose ${cChoice}. ` + determineWinner(choice, cChoice);
    console.log(theResult);
  }

  let theWinner = player1[1] == winScore ? player1[0] : player2[0];
  console.log(`\nGame over. ${theWinner} wins the match!\n`);
  process.exit();
}

main();
