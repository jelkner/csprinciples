# Refactorings

This directory will contain examples of student work and the process of
[code refactoring](https://en.wikipedia.org/wiki/Code_refactoring) to make
make it more readable, more maintainable, and to strive to make it
[clean code](https://cleancoders.com/).

## Examples

* [aslConverter.js](aslConverter.js)
