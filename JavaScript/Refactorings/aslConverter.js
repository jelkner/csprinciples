onEvent("convert", "click", function() {
  var word = getText("input").toUpperCase();
  convertToSign(word);

});

function makeSignUrl(letter, pos) {
  var signUrlParts = [
    ["2/27", "91"], ["1/18", "53"], ["e/e3", "111"], ["0/06", "59"],
    ["c/cd", "78"], ["8/8f", "59"], ["d/d9", "120"], ["9/97", "120"],
    ["1/10", "68"], ["b/b1", "120"], ["9/97", "66"], ["d/d2", "88"],
    ["c/c4", "79"], ["e/e6", "78"], ["e/e0", "92"], ["0/08", "120"],
    ["3/34", "95"], ["3/3d", "58"], ["3/3f", "92"], ["1/13", "91"],
    ["7/7c", "55"], ["c/ca", "58"], ["8/83", "61"], ["1/1d", "120"],
    ["0/0a", "78"]
  ];
  var signURL = "https://upload.wikimedia.org/wikipedia/commons/thumb/";
  signURL += signUrlParts[pos][0] + "/Sign_language_" + letter;
  signURL += ".svg/" + signUrlParts[pos][1] + "px-Sign_language_";
  signURL += letter + ".svg.png";

  return signURL;
}

function convertToSign(word) {
  var letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  for (var i = 0; i < 8; i++) {
    var letterIndex = letters.indexOf(word[i]);
    if (letterIndex !== -1) {
      var url =  makeSignUrl(word[i], letterIndex);
      setProperty("image" + (i + 1), "image", url); 
    } else {
      setProperty("image" + (i + 1), "image", "icon://fa-star-o");
    }
  }
}
