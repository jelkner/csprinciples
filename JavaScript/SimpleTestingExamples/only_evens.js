const DEBUG = true;

if (DEBUG) {
  var assert = require('assert');
  const errorMsg = 'Not even';
  for (let number = 0; number < 11; number++) {
    console.log('the number is ' + number);
    console.assert(number % 2 === 0, 'number %s is not even', number);
  }
}
