const DEBUG = true;

function mySum(n) {
  return 0;
}

if (DEBUG) {
  var assert = require('assert')
  console.log("Testing if mySum(10) returns 55")
  console.assert(mySum(10) === 55);
}
