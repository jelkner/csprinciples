# JS Turtle

The goal here is to use Turtle graphics in the HTML5 canvas in as simple a
way as possible, exposing as much possible of the underlying mechinisms by
which it works.

It would also be nice to be able to aligned as much as possible with the
curriculum in
[Self Paced Introduction to Turtle Programming In App Lab](https://studio.code.org/s/csp3-virtual?section_id=3577173),
but I am willing to sacrifice alignment for simplicity.

After investigating
[Turtle graphics with the HTML5 canvas](https://cnx.org/contents/hTEMulu8@2/Turtle-graphics-with-the-HTML5-canvas),
[JavaScript Turtle Graphics](https://www.turtlegraphics.fun/javascript.html),
and [p5.js](https://p5js.org/) combined with
[TurtleGFX](https://discourse.processing.org/t/turtlegfx-an-open-source-turtle-graphics-library-for-p5-js/22201),
I finally came upon the *Holy Grail*,
[Turtle graphics with the HTML5 canvas](https://cnx.org/contents/hTEMulu8@2/Turtle-graphics-with-the-HTML5-canvas),
which is *exactly* what I was looking for.

Who knows, we may be able to contribute to this last project in some way.
