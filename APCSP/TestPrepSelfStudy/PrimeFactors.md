# Prime Factors

Suppose you have an available procedure called isPrime that works correctly.
The isPrime procedure accepts an integer parameter and returns true if the
number is prime and false otherwise.

## Examples:

- ``isPrime(13)`` returns ``true``.
- ``isPrime(49)`` returns ``false``.
- ``isPrime(2)`` returns ``true``.

We want to implement a procedure called largestPrimeDivisor. The
``largestPrimeDivisor`` method will accept an integer (assumed to be a positive
number greater than 1) and return the largest prime number that divides it.

## Examples:

- ``largestPrimeDivisor(20)`` returns ``5``.
- ``largestPrimeDivisor(26)`` returns ``13``.
- ``largestPrimeDivisor(70)`` returns ``7``.

Which of the following correctly implements the largestPrimeDivisor procedure?

## A
```
PROCEDURE largestPrimeDivisor(num)
{
     divisor ← 2
     REPEAT UNTIL (divisor = num)
     {
                IF(isPrime AND num  MOD divisor = 0)  
                {
                      RETURN(divisor)
                }
                divisor ← divisor + 1
     }
     RETURN(num)
}
```

## B
```
PROCEDURE largestPrimeDivisor(num)
{
     divisor ← 2
     REPEAT UNTIL (divisor = num)
     {
                IF(isPrime(divisor) AND num  MOD divisor = 0)  
                {
                      RETURN(divisor)
                }
                divisor ← divisor + 1
     }
     RETURN(num)
}
```

## C
```
PROCEDURE largestPrimeDivisor(num)
{
     divisor ← num 
     REPEAT UNTIL (divisor = 1)
     {
                IF(isPrime(divisor) AND num  MOD divisor = 0)  
                 {
                      RETURN(divisor)
                }
                divisor ← divisor + 1
     }
     RETURN(1)
}
```

## D
```
PROCEDURE largestPrimeDivisor(num)
{
     divisor ← num
     REPEAT UNTIL (divisor = 1)
     {
                IF(isPrime(divisor) AND num  MOD divisor = 0)  
                {
                      RETURN(divisor)
                 }
                divisor ← divisor - 1
     }
     RETURN(1)
}
```

## Solution

We are interested in the *largest* prime divisor. First let's make sure we
understand these terms.  A
[prime number](https://en.wikipedia.org/wiki/Prime_number) is natural number
that is not the product of two smaller natural numbers. In other words, it
is a number whose only [divisors](https://en.wikipedia.org/wiki/Divisor) are
``1`` and itself.

The algorithms in choices A and B both start looking for the divisor starting
at 2, the *smallest* possibility, not the *largest*. Option A has an additional
problem in that the call to ``isPrime`` requires and argument, so it won't
work at all.  Option B will find the *smallest* prime divisor of ``num``.

Option C begins looking at ``num``, but then *increments* when it should
*decrement*, so it results in an infinite loop.

Option D does what we want, starting with num and *decrementing* until it finds
a prime divisor and returning the first one it finds, which will be the
largest.


## Extension

We can test this on the computer by implementing the two algorithms in
[JavaScript](https://en.wikipedia.org/wiki/JavaScript) and running them in a
[single-page application](https://en.wikipedia.org/wiki/Single-page_application)
 (SPA).

Here is the source code for a working ``isPrime`` function:
```
function isPrime(n) {
  let val = 2;
  let maxPossible = Math.sqrt(n);
  while (val <= maxPossible) {
    if (n % val == 0) return false;
    val++; 
  }
  return true;
}
```

and here is ``largestPrimeDivisor``:

```
function largestPrimeDivisor(num) {
  let divisor = num;

  do {
    if (isPrime(divisor) && (num % divisor == 0)) return divisor;
    divisor--;
  } while (divisor > 1);

  return 1;
}
```

I created SPAs to enable users to run these functions interactively and hosted
them [here](https://jelkner.github.io/itd210/).
