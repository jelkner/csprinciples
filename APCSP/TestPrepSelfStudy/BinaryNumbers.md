# Binary Numbers

## Question 1

Computer systems use binary numbers to represent data that it processes. If
it has a number in memory represented by the binary number ``0010 1001``, and
it is processing an instruction to add the decimal number 21 to it, what is
the resulting number in binary format?

## Solution 1

Using the table of powers of 2:

| 2^7 | 2^6 | 2^5 | 2^4 | 2^3 | 2^2 | 2^1 | 2^0 |
|-----|-----|-----|-----|-----|-----|-----|-----|
| 128 |  64 |  32 |  16 |   8 |   4 |   2 |   1 |

The number 21 as a sum of powers of 2 (in other words, bits) is 16 + 4 + 1,
which written in 8-bit binary is:

``0001 0101``

Using the same table, we can determine the given number in memory to be
32 + 8 + 1, or 41.

The sum would thus be:

```
  0010 1001
+ 0001 0101
  ---------
  0011 1110
```

which is 62 in decimal, and checks with 41 + 21.


##  Question 2

When designing a web page, you see a color you want to use listed in binary as:
``00101110``, ``10001011``, ``01010111``. Which color is it, given the decimal
equivalents (red, green, blue)?

## Solution 2

Using the powers of two table in the previous question, we get the following

### Red

| 128 |  64 |  32 |  16 |   8 |   4 |   2 |   1 |
|-----|-----|-----|-----|-----|-----|-----|-----|
|  0  |  0  |  1  |  0  |  1  |  1  |  1  |  0  |

Giving us 32 + 8 + 4 + 2 = 46

### Green 

| 128 |  64 |  32 |  16 |   8 |   4 |   2 |   1 |
|-----|-----|-----|-----|-----|-----|-----|-----|
|  1  |  0  |  0  |  0  |  1  |  0  |  1  |  1  |

Which is 128 + 8 + 2 + 1 = 139 

### Blue 

| 128 |  64 |  32 |  16 |   8 |   4 |   2 |   1 |
|-----|-----|-----|-----|-----|-----|-----|-----|
|  0  |  1  |  0  |  1  |  0  |  1  |  1  |  1  |

Or 64 + 16 + 4 + 2 + 1 = 87

So our color is rgb(46, 139, 87), or
[SeaGreen](https://en.wikipedia.org/wiki/Web_colors).
