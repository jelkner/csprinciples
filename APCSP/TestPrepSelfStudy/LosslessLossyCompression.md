# Lossless vs. Lossy Compression

## Question 1

Examine the following two compression algorithms that are used to compress
and transmit text:

### Algorithm 1

All vowels are removed from each word with the exception of the first letter of
the word if it is a vowel. For example, the phrase “football players are always
ready to play” would be compressed to “ftbll plyrs ar alwys rdy t ply” and this
compressed text would be transmitted.

### Algorithm 2

Text is compressed by replacing repetitive text with a key symbol listed in a
table. For example, the phrase “they went there to see the players play the
game” could be compressed to “\@y went \@re to see @ #ers # @ game” using a
table that contains two key entries: “@” represents the text “the” and “#”
represents the text “play”. This table would also be transmitted along with the
compressed text.

What is the most important difference between these two algoritms?

## Solution 1

**Algorithm 1** is [lossy](https://en.wikipedia.org/wiki/Lossy_compression)
and **Algorithm 2** is
[lossless](https://en.wikipedia.org/wiki/Lossless_compression).

The [invertible mapping](https://en.wikipedia.org/wiki/Inverse_function):

```
  the <=> @
  play <=> #
```

provides an easy way to reproduce the original text exactly from the
compressed version using the 2nd algorithm, while with algorithm 1 this can
not be guaranteed.
