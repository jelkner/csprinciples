# Binary Numbers Quiz 00

## Question 0

If the 8-bit byte ``0100 1010`` represents an unsigned number, what is its
value in decimal?

## Answer 0

Using the table of powers of 2 and lining up the given 8-bit byte:

| 2^7 | 2^6 | 2^5 | 2^4 | 2^3 | 2^2 | 2^1 | 2^0 |
|-----|-----|-----|-----|-----|-----|-----|-----|
| 128 |  64 |  32 |  16 |   8 |   4 |   2 |   1 |
|-----|-----|-----|-----|-----|-----|-----|-----|
|  0  |  1  |  0  |  0  |  1  |  0  |  1  |  0  |

we get 64 + 8 + 2 = **74** its value in decimal.


## Question 1

How would the decimal number 148 be represented in 8-bit binary?

## Answer 1

Again using the table of powers of 2, we look for the largest power of 2 from
the table less than 148, (128 in this case), subtract it, and then repeat the
process until there is no remainder from the substraction.  Then fill in a
row under the powers of 2 table with a 1 under each value subtracted.

```
  148
 -128
 ----
   20
  -16
  ---
    4
  - 4
  ---
    0
```

| 2^7 | 2^6 | 2^5 | 2^4 | 2^3 | 2^2 | 2^1 | 2^0 |
|-----|-----|-----|-----|-----|-----|-----|-----|
| 128 |  64 |  32 |  16 |   8 |   4 |   2 |   1 |
|-----|-----|-----|-----|-----|-----|-----|-----|
|  1  |  0  |  0  |  1  |  0  |  1  |  0  |  0  |

So the 8-bit binary representation of ``148`` is ``10010100``.


# Binary Numbers Quiz 01

## Question 0

If the 8-bit byte ``0010 0110`` represents an unsigned number, what is its
value in decimal?

## Answer 0

| 2^7 | 2^6 | 2^5 | 2^4 | 2^3 | 2^2 | 2^1 | 2^0 |
|-----|-----|-----|-----|-----|-----|-----|-----|
| 128 |  64 |  32 |  16 |   8 |   4 |   2 |   1 |
|-----|-----|-----|-----|-----|-----|-----|-----|
|  0  |  0  |  1  |  0  |  0  |  1  |  1  |  0  |

32 + 4 + 2 = **38**

## Question 1

How would the decimal number 184 be represented in 8-bit binary?

## Answer 1

```
  184
 -128
 ----
   56
  -32
  ---
   24
  -16
  ---
    8
   -8
  ---
    0
```

| 2^7 | 2^6 | 2^5 | 2^4 | 2^3 | 2^2 | 2^1 | 2^0 |
|-----|-----|-----|-----|-----|-----|-----|-----|
| 128 |  64 |  32 |  16 |   8 |   4 |   2 |   1 |
|-----|-----|-----|-----|-----|-----|-----|-----|
|  1  |  0  |  1  |  1  |  1  |  0  |  0  |  0  |

So the 8-bit binary representation of ``184`` is ``10111000``.


# Binary Numbers Quiz 10

## Question 0

If the 8-bit byte ``1010 0100`` represents an unsigned number, what is its
value in decimal?

##  Question 1

How would the decimal number 134 be represented in 8-bit binary?


# Binary Numbers Quiz 11

## Question 0

If the 8-bit byte ``1100 0001`` represents an unsigned number, what is its
value in decimal?

##  Question 1

How would the decimal number 143 be represented in 8-bit binary?
