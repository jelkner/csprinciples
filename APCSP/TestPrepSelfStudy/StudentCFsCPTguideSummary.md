## CPT Student Guide Summary

You cannot have any outside help, apart from your partner, but the videos and
assessment have to be completed individually. At the end you have to have: the
code, a video of your code running, and written responses to the questions
(basically, a test). The program file has to be a PDF and include all comments.


## The program must include:

- Input from the user, a device, an online data stream, or a file
- At least one list
- At least one procedure (and call(s) to it)
- An algorithm that includes sequencing, selection, and iteration
- Instructions for output based on input and program functionality
 

## The video must demonstrate:

- Input to the program
- At least one aspect of the functionality of the program
- Output of the program
- Video must not include:
    - Any information about yourself
    - Voice narration (captions are fine)

## Video must be:

- .mp4, .mmv, .avi, or .mov format
- ≤ 1 minute long
- ≤ 30MB


## Written responses 

The written response to all the prompts must be < 750 words. You are not
allowed to collaborate with anyone on the written response.


### You have to answer:

- Overall purpose of the program
- Functionality of the program
- Input and output of the program
- How data was stored in a list
- How the data from the list is used
- Identifies the name of the list being used in this response
- Describes what the data contained in the list represent in your program
- Explains how the selected list manages complexity in your program code by
  explaining why your program code could not be written, or how it would be
  written differently, if you did not use the list Defines the
  procedure’s name and return type
- Contains and uses one or more parameters that have an effect on the
  functionality of the procedure
- Implements an algorithm that includes sequencing, selection, and iteration
- Describes in general what the identified procedure does and how it
  contributes to the overall functionality of the program
- Explains in detailed steps how the algorithm implemented in the identified
  procedure works
- Describes two calls to the procedure identified in written response
- Describes what condition(s) is being tested by each call to the procedure
- Identifies the result of each call
 

*You must appropriately acknowledge any media used or it is considered
plagiarism.*


## Before starting the CPT, students should:

- Obtain content knowledge and skills that will help you succeed on the
  performance task. 
- Review the performance task directions and guidelines.
- Brainstorm problems that programming can address, or brainstorm special
  interests that programming can help develop.
- Seek assistance from your teacher or AP Coordinator on defining your focus
  and choice of topics.
- Be prepared to collaborate with peers.
- Practice and discuss the performance task.
- Review the role your teacher can and cannot play in providing assistance
  during the actual performance task.
- Review the scoring guidelines.
- Ensure you know the proper way to cite media or program code.
- Understand the level of detail expected in writing your responses.

## Submission practice / preparation steps:

- Read through the AP Digital Portfolio file submission requirements and
  process.
- Practice creating a video of your program running.
- Practice creating a PDF file of your program code to submit for the
  performance task.
- Understand that you may not revise your work once you have submitted it as
  final to the AP Digital Portfolio.
- We have already done some of these things earlier in the year.


## Once you have started the CPT, you may:

- Collaborate with your partner(s) (NOT for the video and written responses)
- Follow a timeline and schedule
- Seek assistance from your teacher or AP Coordinator on the formation of
  groups and resolution of collaboration issues
- Seek clarification from your teacher or AP Coordinator on the prompts and
  submission requirements
- Work on the CPT outside of designated class time.
- Seek assistance from your teacher or AP Coordinator to resolve technical
  problems that impede work
- Keep a programming journal of the design choices that were made during the
  development of the program code or code segment and the effect of these
  decisions on the program’s function.
