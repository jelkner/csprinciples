# Create Performance Task Survival Guide

*Adapted from the [Code.org](https://code.org/) resource, Create PT Survival
Guide*

## Task Overview

**About the Task:** During the task you will write a program about a topic
entirely of your choosing and then write written responses explaining the
purpose, functionality, data abstractions, and procedural abstractions you
used. You will have at least 12 class hours to complete the task, at the end of
which you'll need to submit:

* A PDF of your program code
* A video of your program running
* Four written responses explaining different parts of your program

**About this Guide:** By completing our course work you should already have the
skills and knowledge necessary to complete the task. While the Create Task
should be a creative opportunity, you'll want to make sure you use your time
wisely and design your program with the task requirements in mind. To make sure
you're able to do that, this guide includes:

* Activities to highligh tricky parts of the task requirements
* Organizers and planning guides to help you think through ideas
* A recommeded schedule for using your 12 hours
* Checklists to make sure your final submission meets all the requirements

**College Board Resources:** This guide is intended to be a companion to
resources provided by the College Board, in particular the task directions and
scoring guidelines. Those documents are the final authority on the requirements
of the project. This guide, however, should help you understand the nuances of
those documents and understand what certain terms mean when designing a program
in Python.

## What is Required of My Program?

You have a lot of freedom to design any project you like for the Create Task.
The task directions include only a few requirements for what you include as
your program. This page summarizes those requirements, highlights the
"takeaways", and explains what they might look like in Python.

### Input / Output Requirements

The video of your program and response 3a requires you to demonstrate input and
output in your program. In Python the most straightforward way to do this is
to use the *command-line interface*. Prompt users to enter input into the
program, process it, then print out the results. 

**Takeaway 1:** Make sure your program has both **input** and **output**.

### List Requirements

Response 3b requires your program to demonstrate your program uses a list to
store multiple pieces of information as well as some code where that list is
"processed". This just means you'll need to create a list somewhere in your
program, and later you'll want to access the information stored in that list.
This list could be a hard-coded list, filled in by user input, or be
information you pull from a data library.

**Takeaway 2:** Make sure your program creates and uses a list of information.

### Function Requirements

Your function needs to include "at least one procedure that uses one or more
parameters to accomplish the program’s intended purpose, and that implements an
algorithm that includes sequencing, selection, and iteration." Sequencing
merely means that there are multiple lines of code that run in order.
Selection means that a conditional, or if-statement, selects between two or
more portions of your program to run. Iteration simply means repeating some
behavior, as in a loop.

**Takeaway 3:** Make sure your program includes a function that has a
parameter, an if-statement, and a loop. 


