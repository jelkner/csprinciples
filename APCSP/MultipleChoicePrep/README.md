# How to Prepare for APCSP Presentation Notes

## Resource Links

The resources below have been evaluated by our class to be of high quality
and effective resources to prepare for the AP exam.

* [Khan Academy: AP Computer Science Principles](https://www.khanacademy.org/computing/ap-computer-science-principles)
* [AP Central: AP CSP Past Exam Questions](https://apcentral.collegeboard.org/courses/ap-computer-science-principles/exam/past-exam-questions)
* [AP CSP Practice Exam](https://www.test-guide.com/ap-computer-science-principles-practice-exam.html)
