# Big Idea 4: Computing Systems and Networks 


## Main ideas

- Built-in **redundancy** enables the Internet to continue working when parts
  of it are not operational.
- Communication on the Internet is defined by **protocol** such as
  TCP/IP that determine how messages to send and receive data are formatted.
- Data sent through the Internet is broken into **packets** of equal size.
- The Internet is **scalable**, which means that new capacity can be quickly
  added to it as demand grows.
- The **World Wide Web** (WWW) is a system that uses the Internet to share
  web pages and data of all types.
- Computing systems can be in **sequential**, **parallel**, and **distributed**
  configurations as they process data.


## Vocabulary

- Bandwidth
- Computing device
- Computing network
- Computing system
- Data stream
- Distributed computing system
- Fault-tolerant
- Hypertext Transfer Protocol (HTTP) 
- Hypertext Transfer Protocol Secure (HTTPS) 
- Internet Protocol (IP) address
- Packets
- Parallel computing system
- Protocols
- Redundancy
- Router
