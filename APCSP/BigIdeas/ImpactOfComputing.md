# Big Idea 5: Impact of Computing 


## Main ideas

- New technologies can have both beneficial and unintended harmful effects,
  including the presence of bias.
- Citizen scientists are able to participate in identification and problem
  solving.
- The digital divide keeps some people from participating in global and local
  issues and events.
- Licensing of people’s work and providing attribution are essential.
- We need to be aware of and protect information about ourselves online.
- Public key encryption and multifactor authentication are used to protect
  sensitive information.
- Malware is software designed to damage your files or capture your sensitive
  data, such as passwords or confidential information.


## Vocabulary

- asymmetric ciphers
- authentication
- bias
- Certificate Authority (CA)
- citizen science
- Creative Commons licensing
- crowdsourcing
- cybersecurity
- data mining
- decryption
- digital divide
- encryption
- intellectual property (IP)
- keylogging
- malware
- multifactor authentication
- open access
- open source
- free software
- FOSS
- PII (personally identifiable information)
- phishing
- plagiarism
- public key encryption
- rogue access point
- targeted marketing
- virus


## Presentaton Topics

- Access to Information: Cloud Computing
- Access to Information: Digital Divide
- Bias in Computing Innovations
- Crowdsourcing and Citizen Science
- Legal and Ethical Concerns: Copyright and Creative Commons
- FOSS: Free and Open Source Software
- Data Mining and Open Access to Data
- Personally Identifiable Information (PII)
- Cryptography and Public Key Encryption
- Cybersecurity and Protecting Personal Data
- Privacy Concerns
