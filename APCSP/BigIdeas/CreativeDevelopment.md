# Big Idea 1: Creative Development

## Main ideas

- Collaboration on projects with diverse team members representing a variety
  of backgrounds, experiences, and perspectives can develop a better computing
  product.
- A *development process* should be used to design, code, and implement
  software that includes using feedback, testing, and reflection.
- Software documentation should include the programs requirements, constraints,
  and purpose.
- Software should be adequately tested before it is released.
- *Debugging* is the process of finding and correcting errors in software.


## Vocabulary

- code segment
- collaboration
- comments
- debugging
- event-driven programming
- incremental development process
- iterative development process
- logic error
- overflow error
- program
- program behavior
- program input
- program output
- prototype
- requirements
- runtime error
- syntax error
- testing
- user interface


## Computing Innovation

A *computer artifact* is anything created using a computer, including apps,
games, images, videos, audio files, 3D-printed objects, and websites.
*Computing innovations* are innovations which include a computer program as a
core part of its functionality.  GPS and digital maps are examples of
computing innovations that build upon the early non-computer innnovation of
map making.


## Development Process 

![Software Development Life Cycle](resources/SDLF.svg)


## Errors

Four types of programming errors need to be understood:

1. Syntax errors
2. Runtime errors
3. Logic errors
4. Overflow errors

Which type of error is this?
```
for i in range(10)
    print(i)
```

Which type of error is this?
```
# Add the numbers from 1 to 10 and print the result
total = 0
for num in range(10)
    total += num 
print(total)
```

Which type of error is this?
```
nums = [3, 5, 8, 0, 9, 11]
result = 1
for num in nums:
    result = result / num
print(result)
```

## Presentaton Topics

- Creativity and Computing Innovations
- Benefits of Collaboration
- Software Development Process 
- Software Testing
- Types of Software Errors
- Software Documentation
