# Big Idea 3: Algorithms and Programming 


## Main ideas

- **Abstractions** are created by removing details and making concepts more
  general. Programs use **data abstraction** and **procedural abstraction**
  to manage complexity.
- All algorithms can be written using a combination of **sequential**,
  **selection**, and **iterative** statements.
- **Variables** hold **values** in programs. **Assignment statements** change
  the value stored in a variable.
- **Procedures** are reusable blocks of code.
- **Return statements** exit a procedure and can send values back from the
  procedure to the calling program.
- **Lists** are collections of data stored in one variable. `FOR EACH` loops
  traverse each item in a list.
- Some algorithms cannot run in a reasonable amount of time, usually for
  large datasets.  **Heuristics** can sometimes be used to find a solution
  that is close enough.
  [Undecidable problems](https://www.khanacademy.org/computing/ap-computer-science-principles/algorithms-101/solving-hard-problems/a/undecidable-problems)
  are problems that should give a "yes" or "no" answer for which no
  algorithm exists that can answer correctly on all inputs.
- **Simulations** are abstractions to test theories in a safer, controlled,
  faster environment.


## Vocabulary

- algorithm
- API
- argument
- arithmetic operators
- assignment statement
- binary search
- boolean values
- clarity
- code statement
- concatenation
- condition
- data abstraction
- decidable problem
- decision problem
- efficiency
- element
- expression
- heuristic
- iterative
- libary
- linear search
- lists
- logical operators
- modularity
- modulus
- optimization problem
- parameter
- procedural abstraction
- procedure
- pseudocode
- readability
- relational operators
- `RETURN` statement
- selection statement
- sequential statement
- simulation
- string
- substring
- traverse
- undecidable program
- variable

