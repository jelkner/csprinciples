# Access to Information: Cloud Computing

[Investopedia](https://www.investopedia.com/terms/c/cloud-computing.asp)
defines **Cloud Computing** as the delivery of different services such as
data storage, servers, databases, networking, and software through the
Internet.

The use of the term "cloud" is somewhat misleading. As the [saying on the
tech humor t-shirt](https://www.teeshirtpalace.com/products/tht9808979-tech-humor-there-is-no-cloud-just-someone-elses-computer-t-shirt?color=black&size=L&gclid=EAIaIQobChMIiLi7yOu4_AIVjODICh0zkgBYEAQYAyABEgJGjPD_BwE) reads,
"There is no cloud. It's just someone else's computer."

But the idea of having *someone else* manage services on a computer which we
do not have to manager ourselves offers us a great deal of power and
convenience, enabling us end users the ability to use the services provided
for our ultimate objectives without the overhead of having to manage them
ourselves.

We should, however, be conscious of the costs, either explicit or implicit,
we are paying for these services.  Companies that provide "free" services,
such as Google Docs are actually exchanging the service they provide for the
rights to use the data the users provide them.  This "big data" is very
valuable indeed, and is what has enabled companies like
[Alphabet, Inc.](https://en.wikipedia.org/wiki/Alphabet_Inc.) to generate
billions of dollars in revenue from the sale of that data.

It is also possible to host "cloud" services on your own computer. The
FOSS software [Nextcloud](https://en.wikipedia.org/wiki/Nextcloud) provides
file hosting and collaboration services. It is possible to run a Nextcloud
server on a [Raspberry Pi](https://en.wikipedia.org/wiki/Raspberry_Pi) at
your home.

The key characteristic captured by the cloud metaphor in "cloud computing" is
that the services are provided to any device, anywhere, as long as the device
can connect to the Internet.
