# Power On!

Discussion questions for the graphic novel
[Power On!](https://mitpress.mit.edu/9780262543255/power-on/) by Jean J. Ryoo
and Jane Margolis, illustrated by Charis JB.

1. The story in **Power On!** presents the protagonists with several **Impact
   of Computing** issues.  Choose at least two of these that resonated with
   you and briefly describe them.

2. In the story, Christine and Jon, Antonio, and Taylor are going to three
   different high schools.  The book uses this as a plot device to explore what
   issue?  How do their schools differ in regard to it?

3. How does the story end?  What are the four protagonists doing?  Why?
