# Big Ideas

The 5 *Big Ideas* in the AP CS Principles curriculum are:

1. [Creative Development](CreativeDevelopment.md)
2. [Data](Data.md)
3. Algorithms and Programming
4. [Computing Systems and Networks](ComputingSystemsAndNetworks.md)
5. [Impact of Computing](ImpactOfComputing.md)
