# APCSP Course Syllabus 


## Preliminaries: Start of the School Year

Set up students with a Python software development environment, including:

* A basic familiarity with the Unix shell.
* Facility with an editor / IDE.
* Ability to use git to maintain a source code repository.


## Student Expectations

Students enter Advanced Placement Computer Science Principles (APCSP) with
widely varying previous experience. Some students have had no formal computer
science training. Others have taken an introduction to HTML and CSS or an
introduction to programming offered at school. Some students have experienced
summer programs that included introductory programming.

No prior experience programming is expected on entering this course. It is
expected that all students entering APCSP will be knowledgeable about the
topics taught in Algebra I, including functions, variables, and expressions.


## Course Overview

AP Computer Science Principles is designed to encourage a diverse group of
students to explore computer science. Rather than limiting this introductory
study to a single big idea — algorithms and programming — this course
introduces students to a broad set of big ideas. These big ideas, which include
algorithms and programming, are creative development, data, computing systems
and networks, and the impact of computing. In addition, this course emphasizes
the use of computational thinking practices for effective learning experiences
and problem solving. These practices include computational solution design,
algorithms and program development, abstraction in program development, code
analysis, computing innovations, and responsible computing.

At our school, AP CSP is explored using an hands-on, project based learning
approach.  Students build their knowledge and understanding through
participation in a wide variety of activities and explorations. Before, during,
and after explorations, connections are made to the five big ideas at the core
of the course.  Activities encourage students to regularly apply the six
computational thinking practices to their work.

It is not expected that students will fully understand any significant computer
science topic after their first exposure to the subject matter. Instead,
students will reflect upon and expand their understanding as they revisit
related topics throughout the course.

Assessments will build on prior knowledge, and student performance expectations
for specific topics will increase with each assessment.  The primary
programming language used in this course is Python. However, students
are exposed to other programming options as well.

## Big Ideas

BI1: Creative Development (CRD)
BI2: Data (DAT)
BI3: Algorithms and Programming (AAP)
BI4: Computing Systems and Networks (CSN)
BI5: Impact of Computing (IOC)

## Computational Thinking Practices

CTP1: Computational Solution Design (Design and Evaluate)
CTP2: Algorithms and Program Development (Develop)
CTP3: Abstraction in Program Development (Develop)
CTP4: Code Analysis (Analyze)
CTP5: Computing Innovations (Investigate)
CTP6: Responsible Computing (Contribute)


## Text and Supplemental Materials

1. [CS Principles: Big Ideas in Programming](http://www.openbookproject.net/books/StudentCSP/)
2. [Power On!](https://mitpress.mit.edu/books/power) by Jean J. Ryoo and
   Jane Margolis


