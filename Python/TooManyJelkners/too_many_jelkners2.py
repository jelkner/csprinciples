from PIL import Image


def write_on_big_canvas(canvas, img, start):
    pixels = img.load()
    w = img.size[0]
    h = img.size[1]

    canvas_pixels = canvas.load()

    for col in range(w):
        for row in range(h):
            canvas_x = start[0] + col
            canvas_y = start[1] + row
            if canvas_x < canvas.width and canvas_y < canvas.height:
                r, g, b = pixels[col, row]
                canvas_pixels[canvas_x, canvas_y] = (r, g, b)


def mirror(img):
    img2 = img.copy()
    pixels = img2.load()
    size = min(img2.size[0], img2.size[1])

    for col in range(size):
        for row in range(size):
            pixels[row, col] = pixels[col, row]

    return img2


# Create an image from a file
img = Image.open("jelkner_dc_square.jpg")

# Create an image 3 times the width and height of the original
canvas = Image.new(mode="RGB", size=(3 * img.width, 3 * img.height))

write_on_big_canvas(canvas, img, (0, 0))
write_on_big_canvas(canvas, mirror(img), (img.width, 0))

canvas.show()
