# Using Python in a Computer Science Principles Course

Python is certainly among the best choices for learning to program. This
directory holds resources related to using it in a CS Principles course.

## Links

* [Stanford Karel](https://github.com/TylerYep/stanfordkarel)
