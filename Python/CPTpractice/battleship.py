import os 
import random


def display_board(board, rows, cols, message=""):
    os.system('clear')
    top_heading = [f' {col}' for col in cols]
    print(' ' + ''.join(top_heading))
    for row in range(10):
        for col in range(10):
            if col == 0:
                print(rows[row], end='')
            if col == 9:
                print(f' {board[row][col]}')
            else:
                print(f' {board[row][col]}', end='')
    if message:
       print(f"\n{message}")


def place_ship(ship, board):
    """
    Tries to place ship randomly on board. It fails if any of the spaces it
    tries to use are already occupied, and returns False. If all the spaces
    are available, it sets them to the appropriate ship character and returns
    True.
    """
    # choose starting position for ship
    row = random.randint(0, 10 - ship[1])
    col = random.randint(0, 10 - ship[1])
    # choice an orientation
    vertical = random.randint(0, 1)
    if vertical:
        # check positions clear
        for r in range(ship[1]):
            if board[row + r][col] != '-':
                return False
        # all clear if we get here, so place ship
        for r in range(ship[1]):
            board[row + r][col] = ship[2]
    else: # do the same for vertical placement
        for c in range(ship[1]):
            if board[row][col + c] != '-':
                return False
        for c in range(ship[1]):
            board[row][col + c] = ship[2]
    return True


def place_ships(ships):
    """
    Creates a ship board and repeatedly calls place_ship for each ship to
    randomly place them on the board. Returns the setup board when finished.
    """
    board = []
    for row in range(10):
        board.append(list('----------'))
    index = 0
    while index < len(ships):
        if place_ship(ships[index], board):
            index += 1
    return board


def game_over(ships):
    """
    Returns True if the number of hits on each ship equals its length, False
    otherwise.
    """
    for ship in ships:
        if ship[1] != ship[3]:
            return False
    return True


def ship_index(ch, ships):
    """
    Returns the index of the ship with ship character ch.
    """
    for i in range(len(ships)):
        if ch == ships[i][2]:
            return i
    raise ValueError


def play_game():
    shot_board = []
    for row in range(10):
        shot_board.append(list('----------'))
    ships = [
        ['Carrier', 5, 'C', 0],
        ['Battleship', 4, 'B', 0],
        ['Destroyer', 3, 'D', 0],
        ['Submarine', 3, 'S', 0],
        ['Patrol Boat', 2, 'P', 0]
    ]
    ship_board = place_ships(ships)
    rows = "abcdefghij"
    cols = "0123456789"
    shots = 0
    message = ""

    while not game_over(ships):
        display_board(shot_board, rows, cols, message)
        shot = input("\nType a position to hit: ")
        if shot == 'cheat':
            display_board(ship_board, rows, cols)
            input("\nTake a peek, and press any key to return to play...")
            continue
        else:
            while len(shot) != 2 or shot[0] not in rows or shot[1] not in cols:
                message = ""
                display_board(shot_board, rows, cols, "Invalid input")
                shot = input("\nPlease try again: ")
        # we have a valid shot, process it
        shots += 1
        row = rows.index(shot[0])
        col = cols.index(shot[1])

        ship_board_ch = ship_board[row][col]
        shot_board_ch = shot_board[row][col]
        if ship_board_ch == '-' and shot_board_ch == '-':          # miss
            shot_board[row][col] = 'o'
            message = "Miss!"
        elif shot_board_ch in "ox":
            message = "Wasted shot! No point hitting the same place twice..."
        else:                                                      # hit
            shot_board[row][col] = 'x'
            ship = ships[ship_index(ship_board_ch, ships)]
            ship[3] += 1
            if ship[1] == ship[3]:
                message = f"Hit! You sunk the {ship[0]}."
            else:
                message = "Hit!"

    message = f"You won, and it only took you {shots} shots!"
    display_board(shot_board, rows, cols, message)


if __name__ == "__main__":
    play_game()
