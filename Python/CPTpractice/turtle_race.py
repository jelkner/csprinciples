import random
from turtle import *


def place_turtle(color, pos, turtles):
    turtle = Turtle(shape='turtle')
    turtle.penup()
    turtle.fillcolor(color)
    turtle.goto(-460, pos)
    turtles.append(turtle)


def setup():
    global turtles
    global screen
    
    num_turtles = int(numinput("Turtles", "How many turtles?"))

    turtles = []
    screen = Screen()
    screen.setup(1000, 100 * num_turtles + 80)
    title("Turtle Race!")

    start_pos = 100 * num_turtles // 2 - 40
    next_pos = start_pos

    # Draw the finish line
    line_turtle = Turtle()
    line_turtle.color("red")
    line_turtle.pensize(10)
    line_turtle.penup()
    line_turtle.goto(460, 100 * num_turtles // 2)
    line_turtle.right(90)
    line_turtle.pendown()
    line_turtle.forward(100 * num_turtles)
    line_turtle.hideturtle()

    for n in range(num_turtles):
        color = textinput(
            "Color Picker",
            f"What color for turtle number {n+1}?"
        )
        place_turtle(color, next_pos, turtles)
        next_pos -= 100


def race():
    global turtles
    global screen

    racing = True

    while racing:
        for turtle in turtles:
            turtle.forward(random.randint(0, 10))
            if turtle.xcor() >= 460:     # We have a winner, stop race
                racing = False
                break
        
        if not racing:                   # Announce winner
            win_turtle = Turtle()
            win_turtle.write(
                "We have a winner!",
                move=False,
                align="center",
                font=("Georgia", 30, "normal")
            )

    screen.exitonclick()


if __name__ == '__main__':
    setup()
    race()
