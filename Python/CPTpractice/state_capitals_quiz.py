import os
import random


def load_data():
    f = open('state_capitals.dat', 'r')
    lines = f.readlines()
    f.close()
    state_capitals = [] 
    for line in lines:
        state_and_cap = line.split(':')
        state_capitals.append((state_and_cap[0], state_and_cap[1][:-1]))
    return state_capitals


def ask_question(data):
    """
    Ask one of two types of question: capital given state or state given
    capital.
    """
    state_or_capital_index = random.randint(0, 1)
    state_and_capital = random.choice(data)
    state_or_capital = state_and_capital[state_or_capital_index]
    forms = (
        f"What is the capital of {state_or_capital}? ",
        f"Which state has {state_or_capital} as its capital? "
    )
    question = forms[state_or_capital_index]
    # choose the other element in the pair
    correct = state_and_capital[-state_or_capital_index + 1] 
    answer = input(question)
    if answer == correct:
        print("Great job!")
    else:
        print("Sorry, better luck next time.")


def run_state_capitals_quiz():
    ask_question(load_data())


if __name__ == '__main__':
    run_state_capitals_quiz()
