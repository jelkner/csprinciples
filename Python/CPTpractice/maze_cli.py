import os
import random


class Maze:
    def __init__(self):
        f = open('mazes.dat', 'r')
        lines = f.readlines()
        f.close()
        # Randomly choose one of the 8 mazes from the dat file and load it.
        self.maze_num = random.randint(1, 8)
        start_line = lines.index(f"## {self.maze_num} ##\n") + 1
        self.lines = []
        for line in range(start_line, start_line+22):
            self.lines.append(lines[line][:-1])
        self.player_row = 0
        num_lines = len(self.lines)
        while self.player_row < num_lines:
            if "P" in self.lines[self.player_row]:
                break
            self.player_row += 1
        self.player_col = self.lines[self.player_row].index("P")
        self.direction = None

    def draw_and_get_move(self):
        os.system('clear')
        for line in self.lines:
            print(line)
        moved = {
            None: "",
            "h": "Left key pressed. ",
            "j": "Down key pressed. ",
            "k": "Up key pressed. ",
            "l": "Right key pressed. "
        }
        key_pressed = input(f"\n{moved[self.direction]}Enter h, j, k, or l: ")
        while key_pressed not in "hjkl": 
            os.system('clear')
            for line in self.lines:
                print(line)
            key_pressed = input("\nBad input! Please enter h, j, k, or l: ")
        self.direction = key_pressed
        self.make_move()

    def make_move(self):
        d = self.direction
        r, c = self.player_row, self.player_col
        if d == "h" and self.lines[r][c-1] == ' ':       # move left is clear 
            nline = self.lines[r]
            nline = f"{nline[:c-1]}P {nline[c+1:]}"
            self.lines[r] = nline
            self.player_col -= 1
        elif d == "l" and self.lines[r][c+1] == ' ':     # move right is clear
            nline = self.lines[r]
            nline = f"{nline[:c]} P{nline[c+2:]}"
            self.lines[r] = nline
            self.player_col += 1
        elif d == "j" and self.lines[r+1][c] == ' ':     # move down is clear
            nl1 = self.lines[r]
            nl2 = self.lines[r+1]
            self.lines[r] = f"{nl1[:c]} {nl1[c+1:]}"
            self.lines[r+1] = f"{nl2[:c]}P{nl2[c+1:]}"
            self.player_row += 1
        elif d == "k" and self.lines[r-1][c] == ' ':     # move up is clear
            nl1 = self.lines[r-1]
            nl2 = self.lines[r]
            self.lines[r-1] = f"{nl1[:c]}P{nl1[c+1:]}"
            self.lines[r] = f"{nl2[:c]} {nl2[c+1:]}"
            self.player_row -= 1

    def finished(self):
        if self.direction == "h" and self.player_col == 0:
            return True
        if self.direction == "l" and self.player_col == len(self.lines[0]) - 1:
            return True
        if self.direction == "j" and self.player_row == len(self.lines) - 1:
            return True
        if self.direction == "k" and self.player_row == 0:
            return True
        return False


if __name__ == "__main__":
    maze = Maze()
    while not maze.finished():
        maze.draw_and_get_move()
    os.system('clear')
    print("YOU WIN!")
