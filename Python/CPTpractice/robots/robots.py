from gasp import *
import random
import sys 


class Player:
    pass


class Robot:
    pass


def place_thing(thing):
    thing.x = random.randint(0, 63)
    thing.y = random.randint(0, 47)


def safely_place_thing(list_of_things, is_player=False):
    thing = Player() if is_player else Robot()
    place_thing(thing)

    while collided(thing, list_of_things):
        place_thing(thing)

    if isinstance(thing, Player):
        thing.shape = Circle((10*thing.x+5, 10*thing.y+5), 5, filled=True)
    else:
        thing.shape = Box((10*thing.x, 10*thing.y), 10, 10)

    return thing 


def place_robots(numbots):
    robots = []

    while len(robots) < numbots:
        robot = safely_place_thing(robots) 
        robot.junk = False
        robots.append(robot)

    return robots


def move_player(player, robots):
    key = update_when("key_pressed")

    while key in ["KP_5", "s", "k"]:
        remove_from_screen(player.shape)
        place_thing(player)
        while collided(player, robots):
            place_thing(player)
        player.shape = Circle(
            (10*player.x+5, 10*player.y+5), 5, filled=True
        )

        key = update_when("key_pressed")

    if key in "1zm":
        if player.x > 0:
            player.x -= 1
        if player.y > 0:
            player.y -= 1
    elif key in ["KP_2", "x", "comma"] and player.y > 0:
        player.y -= 1
    elif key in ["KP_3", "c", "period"]:
        if player.x < 63:
            player.x += 1
        if player.y > 0:
            player.y -= 1
    elif key in ["KP_4", "a", "j"] and player.x > 0:
        player.x -= 1
    elif key in ["KP_6", "d", "l"] and player.x < 63:
        player.x += 1
    elif key in ["KP_7", "q", "u"]:
        if player.x > 0:
            player.x -= 1
        if player.y < 47:
            player.y += 1
    elif key in ["KP_8", "w", "i"] and player.y < 47:
        player.y += 1
    elif key in ["KP_9", "r", "o"]:
        if player.x < 63:
            player.x += 1
        if player.y < 47:
            player.y += 1

    move_to(player.shape, (10*player.x+5, 10*player.y+5))


def move_robots(robots, player):
    for robot in robots:
        if not robot.junk:
            if robot.x > player.x:
                robot.x -= 1
            elif robot.x < player.x:
                robot.x += 1

            if robot.y > player.y:
                robot.y -= 1
            elif robot.y < player.y:
                robot.y += 1

            move_to(robot.shape, (10 * robot.x, 10 * robot.y))


def collided(thing1, list_of_things):
    for thing2 in list_of_things:
        if thing1 == thing2:          # things can't collide with themselves
            continue
        if thing1.x == thing2.x and thing1.y == thing2.y:
            return True
    return False


def still_surviving(robots):
    for robot in robots:
        if not robot.junk:
            return True
    return False


def check_collisions(player, robots):
    # Handle player crashes into robot
    if collided(player, robots):
        Text("You've been caught!", (320, 240), size=20)
        sleep(3)
        clear_screen()
        Text("Game Over", (320, 240), size=26)
        sleep(2)
        end_graphics()
        sys.exit()

    # Handle robots crashing into each other
    for robot in robots:
        if not robot.junk and collided(robot, robots):
            robot.junk = True
            remove_from_screen(robot.shape)
            robot.shape = Box(
                (10*robot.x, 10*robot.y), 10, 10, filled=True
            )

    if not still_surviving(robots):
        finished_level = True
        clear_screen()
        Text("You beat the level!", (320, 240), size=20)
        sleep(3)
        return True


begin_graphics()
numbots = 4

while True:
    finished_level = False

    robots = place_robots(numbots)
    player = safely_place_thing(robots, is_player=True)

    while not finished_level:
        move_player(player, robots)
        move_robots(robots, player)
        finished_level = check_collisions(player, robots)

    clear_screen()
    if numbots < 17:
        numbots *= 2
    else:
        Text("You win!", (280, 240), size=26)
        sleep(3)
        end_graphics()
        sys.exit()
