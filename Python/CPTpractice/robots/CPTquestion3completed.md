# Written Response (Question 3) for Create Performance Task

## 3a. Provide a written response that does all three of the following:

1. Describes the overall purpose of the program
>
> The program implements a game in which the user controls the movement
> of a player being chased by a swarm of robots. The player's goal is to
> avoid being caught by the robots while leading them to collide with each
> other.
>
2. Describes what functionality of the program is demonstrated in the
   video.
>
> The video shows two levels of the game being played.  In the first level
> the player successfully leads all robots to collide with each other,
> winning the level. In the second level the player is caught by a robot
> and the game ends.
>
3. Describes the input and output of the program demonstrated in the
   video.
>
> The inputs to this program are key presses made by the user that cause
> the player to move. The output of the program is the updated screen
> showing the player in new location in response to the user's key press,
> as well as each of the live robots in a new location given by their
> movement toward the player on each turn.
>

## 3b. Provide a written response to the following:
 
Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

1. The first program code segment must show how data have been
   stored in the list.
   ![using_lists1](images/using_lists1.png)

2. The second program code segment must show the data in the same list being
   used, such as creating new data from the existing data or accessing
   multiple elements in the list, as part of fulfilling the program’s purpose.
   ![using_lists2](images/using_lists2.png)


## Then provide a written response that does all three of the following: 

3. Identifies the name of the list being used in this response
>
> An empty list, `robots`, is created on line 35.  It is populated with
> Robot instances by repeatedly calling `safely_place_thing`, which creates
> a robot object with a location (`x` and `y` attribute pair) not taken by any
> previously placed robot, that is then appended to the list on line 40. Once
> the desired number of robots (`numbots`) are placed, the list is returned.
>
4. Describes what the data contained in the list represent in your program
>
> The `robots` list contains a collection of `Robot` instances. Each `robot`
> has a location (`x` and `y` attributes), a `shape`, which is a graphical
> `Box` instance provided by the GASP library, and a boolean attribute,
> `junk`, set to `False` if the robot is "alive" and `True` if it is "dead" as
> a result of colliding with another robot.
>
5. Explains how the selected list manages complexity in your program code by
   explaining why your program code could not be written, or how it would be
   written differently, if you did not use the list
>
> Each time the user causes the player to move, through pressing a key,
> *all* the robots need to move toward the player. This is accomplished by
> storing the robots in a list, and then iterating over the list using a `for`
> loop, comparing the location of each robot in turn to the location of the
> player, and changing its `x` and `y` attributes to be one unit closer to the
> player. This task would not be managable without the reduction in complexity
> from the *data abstraction* provided by the list.
>

## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.

1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedure’s name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration
   ![procedure1](images/procedure1.png)

2. The second program code segment must show where your student-developed
   procedure is being called in your program.
   ![procedure2](images/procedure2.png)

## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program
>
> The `safely_place_thing` procedure creates either a `Player` or a `Robot`
> in a location not already occupied.
>
4. Explains in detailed steps how the algorithm implemented in the identified
   procedure works. Your explanation must be detailed enough for someone else
   to recreate it.
>
> First, create either a `Player` instance or a `Robot` instance depending on
> the value of the boolean paramenter `is_player` (line 20). Next pass this
> instance to `place_thing`, which will set random `x` and `y` attributes on it
> within the dimensions of the game board (0 to 63 for x and 0 to 47 for y).
> Next pass the instance to the boolean `collided` procedure as the condition
> of a `while` loop and repeat the `place_thing` call in the body of the loop
> until the instance is not in a location already occupied (i.e. having the
> same (`x` and `y` attributes as another object in the list).  Finally, once
> safely placed, create the appropriate `shape` instance for the object, either
> `Circle` for a `Player` or a `Box` for a `Robot` (lines 26 to 29) and then
> return the safely placed object to the caller (line 31).
>

## 3d. Provide a written response that does all three of the following:

1. Describes two calls to the procedure identified in written response 3c. Each
   call must pass a different argument(s) that causes a different segment of
   code in the algorithm to execute.

   First call:
   >
   > One line 158, `player = safely_place_thing(robots, is_player=True)`, the
   > procedure is called with the boolean parameter `is_player` set to `True`.
   > this will cause the procedure to create a `Player` instance, safely place
   > it, and then set its shape to `Circle` (line 27).

   ![procedure3](images/procedure3.png)

   Second call:
   >
   > The statement `robot = safely_place_thing(robots)` is called inside the
   > body of a `while` loop on line 38 to create `Robot` instances to
   > populate the `robots` list.

2. Describes what condition(s) is being tested by each call to the procedure

   Condition(s) tested by the first call:
   >
   > The first call tests whether the thing to be created is a `Player`, and
   > since it is (the boolean parameter is set to `True`) it creates a `Player`
   > instance, finds a safe location for it, and then adds its appropriate
   > shape (a `Circle`). 

   Condition(s) tested by the second call:
   >
   > The second call tests whether the thing to be created is a `Player`, and
   > since it isn't (the boolean parameter has its default value `False`) it
   > creates a `Robot` in an unoccupied location with the appropriate shape
   > (a `Box` in this case).

3. Identifies the result of each call

   Result of the first call:
   >
   > The result of the first call is a single `Player` instance, safely placed
   > so that it is not in the same location as any of the `Robots` in the
   > `robots` list.
   
   Result of the second call:
   >
   > The second call (which actually needs to be executed first), creates a
   > `Robot` instance in a location not occupied by any other `Robots` in the
   > `robots` list.  This call is made repeatedly until the `robots` list
   > contains the desired number of `Robots`.
