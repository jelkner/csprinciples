import random
from gasp import *


class Maze:
    def __init__(self):
        f = open('mazes.dat', 'r')
        lines = f.readlines()
        f.close()
        # Randomly choose one of the 8 mazes from the dat file and load it.
        self.maze_num = random.randint(1, 8)
        start_line = lines.index(f"## {self.maze_num} ##\n") + 1
        self.lines = []
        for line in range(start_line, start_line+22):
            self.lines.append(lines[line][:-1])
        row = 0
        num_lines = len(self.lines)
        while row < num_lines:
            if "P" in self.lines[row]:
                break
            row += 1
        col = self.lines[row].index("P")
        self.direction = None
        begin_graphics(440, 440, title=f"Maze {self.maze_num}")
        self.player = Circle(
            (20*col+10, 430-20*row),
            10,
            color=color.RED,
            filled=True
        )
        self.player.row = row
        self.player.col = col

    def draw_and_get_move(self):
        width = len(self.lines)
        height = len(self.lines[0])
        for row in range(height):
            for col in range(width):
                if self.lines[row][col] == "X":
                    Box((20*col, 420-20*row), 20, 20, filled=True)
        self.direction = update_when('key_pressed') 
        self.make_move()

    def make_move(self):
        d = self.direction
        r, c = self.player.row, self.player.col
        if d == "h" and self.lines[r][c-1] == ' ':       # move left is clear 
            self.player.col -= 1
            move_by(self.player, -20, 0)
        elif d == "l" and self.lines[r][c+1] == ' ':     # move right is clear
            self.player.col += 1
            move_by(self.player, 20, 0)
        elif d == "j" and self.lines[r+1][c] == ' ':     # move down is clear
            self.player.row += 1
            move_by(self.player, 0, -20)
        elif d == "k" and self.lines[r-1][c] == ' ':     # move up is clear
            self.player.row -= 1
            move_by(self.player, 0, 20)
        self.direction = update_when('key_pressed') 

    def finished(self):
        if self.direction == "h" and self.player.col == 0:
            return True
        if self.direction == "l" and self.player.col == len(self.lines[0]) - 1:
            return True
        if self.direction == "j" and self.player.row == len(self.lines) - 1:
            return True
        if self.direction == "k" and self.player.row == 0:
            return True
        return False


if __name__ == "__main__":
    maze = Maze()
    maze.draw_and_get_move()
    while not maze.finished():
        maze.make_move()
    clear_screen()
    Text("You win!", (220, 220), color=color.RED, size=36)
    update_when('key_pressed')
    end_graphics()
