import random

students = [
    "Chrisitian", "Emory", "Grant", "Xander", "Blue", "Anupama", "Mayah",
    "Kiersten", "Lary", "Sean", "Eimi", "Jeff", "Gabriel", "Evan",
    "Kellan", "Rockwell", "Alex", "Jack", "Ronan", "Noah", "Toby",
    "Conrad", "Jake"
]
fruits = [
    "apples", "bananas", "cherries", "dates", "elderberries", "figs",
    "grapes", "hackberries", "imbes", "jackfruits", "kiwis", "limes",
    "mangos", "nectarines", "oranges", "papayas", "quinces", "raspberries",
    "strawberries", "tamarinds", "ugnis", "watermellons", "xiguas",
    "yangmeis", "zucchinis"
]
unordered_nums = [
    73, 99, 77, 9, 30, 73, 97, 96, 47, 20, 16, 54, 23, 47, 19, 7, 97,
    82, 91, 37
]
ordered_nums = [
    1, 2, 6, 8, 12, 13, 37, 43, 52, 61, 72, 80, 81, 81, 82, 87, 88, 93, 94, 97
]
random_unordered_nums = [random.randint(1, 100) for i in range(20)]
random_ordered_nums = sorted([random.randint(1, 100) for i in range(20)])
