def linear_search(target, values, verbose=False):
    """
      >>> linear_search(5, [1, 0, 4, 5, 9])
      3
      >>> linear_search("Blu", ["Eimi", "Noah", "Jack", "Lary", "Blu", "Jake"])
      4
      >>> linear_search('a', ['x', 'y', 'z'])
      -1
      >>> linear_search(87, [0, 2, 4, 5, 9, 11, 13, 17, 23, 52, 87, 104])
      10
      >>> linear_search(5, [1, 0, 4, 5, 9], True)
      Looking at element 0. 5 is not 1.
      Looking at element 1. 5 is not 0.
      Looking at element 2. 5 is not 4.
      Looking at element 3. 5 is 5. Found it!
      3
    """
    for i, value in enumerate(values):
        if target == value:
            if verbose:
                print(
                    f"Looking at element {i}. {target} is {value}. Found it!"
                )
            return i
        if verbose:
            print(f"Looking at element {i}. {target} is not {value}.")
    if verbose:
        print("Item not in list.")
    return -1 


def binary_search(target, values, verbose=False):
    """
      >>> from example_data import *
      >>> binary_search('kiwis', fruits)
      10
      >>> binary_search('apples', fruits)
      0
      >>> binary_search('grapefruit', fruits)
      -1
    """
    iter_num = 0
    l = 0
    h = len(values) - 1

    while h > l:
        m = (h + l) // 2
        iter_num += 1
        if verbose:
            print(f"l: {l}  h:{h}  m:{m}  iteration: {iter_num}")
        if target == values[m]: return m
        if target < values[m]: h = m - 1
        elif target > values[m]: l = m + 1

    return -1


if __name__ == "__main__":
    import doctest
    doctest.testmod()
