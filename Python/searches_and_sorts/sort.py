def bubble_sort(values):
    vcopy = values[:]
    print(vcopy)
    for last in range(len(vcopy), 0, -1):
        swaps = False 
        for i in range(last-1):
            if vcopy[i] > vcopy[i+1]:
                vcopy[i], vcopy[i+1] = vcopy[i+1], vcopy[i] 
                swaps = True
        if not swaps: break 
        print(vcopy)


if __name__ == '__main__':
    bubble_sort([8, 2, 9, 11, 0, 5, 17, 6])
