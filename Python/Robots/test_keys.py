from gasp import *


def test_keys():
    key = update_when("key_pressed")

    while key != "Pause":
        clear_screen()
        outstr = f"You the {key} key."
        offset = len(outstr)
        Text(f"You the {key} key.", (500-offset, 150), size=36)

        key = update_when("key_pressed")


begin_graphics(1000, 300)
test_keys()
end_graphics()
