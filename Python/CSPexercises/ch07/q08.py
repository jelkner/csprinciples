"""
NOTE: Talk about str.join method and slicing strings.
"""
a_string = ""
a_list = ["I", "like", "to", "eat", "pizza"]

for word in a_list:
    a_string += f"{word} "

print(a_string[:-1])
print(" ".join(a_list))
