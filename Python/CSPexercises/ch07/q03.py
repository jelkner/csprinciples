"""
NOTE: Talk about *function definitions* and *function calls*, and the
      corresponding relationship between *parameters* and *arguments*.
"""

def sum_list(num_list):
    total = 0
    for n in num_list:
        total += n
    return total

my_nums = [3, 42, 87, 12, 9]
print(f"\nThe sum of the numbers in {my_nums} is {sum_list(my_nums)}.")
