"""
NOTE: Add an expected result to the question.
"""

def who_knows_why(num):
    evens = [n % 2 == 0 for n in range(num)]
    odds = [n % 2 == 1 for n in range(num)]
    evens_sum = sum(evens)
    odds_prod = 1
    for n in odds:
        odds_prod *= n
    return (odds_prod - evens_sum) / ((odds_prod + evens_sum) / 2)

print(f"And the returned value is: {who_knows_why(10)}")
