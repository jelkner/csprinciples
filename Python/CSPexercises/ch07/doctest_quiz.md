# Chapter 7 Doctest Quiz

Write code to make each of the following doctests pass. Assume:
```
if __name__ == "__main__":
    import doctest
    doctest.testmod()
```
is at the bottom of the file.


1. *From question 3*.
```
def sum_list(num_list):
    """
      >>> sum_list([1, 1, 1])
      3
      >>> sum_list([10, 1, 1])
      12
      >>> sum_list([10, 3, -1, 8])
      20
    """
```

2. *From question 7*.
```
def mult_list(num_list):
    """
      >>> mult_list([1, 1, 1])
      1
      >>> mult_list([10, 2, 3])
      60
      >>> mult_list([10, 3, -1, 8])
      -240
    """
```

3. *From question 12*.
```
def factorial(n):
    """
      >>> factorial(5)
      120
    """
```
