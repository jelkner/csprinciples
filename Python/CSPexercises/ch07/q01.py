"""
NOTE: Talk about the range function returning an iterator object, and share
      how passing it to the list function turns it into a list.
"""
sum = 0
things_to_add = list(range(1, 6))

for number in things_to_add:
    print(f"Adding {number} to sum...")
    sum += number

print(f"\nThe sum of the numbers from 1 to 5 is {sum}.")
