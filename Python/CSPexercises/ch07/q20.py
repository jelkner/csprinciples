"""
NOTE:  Use this example to introduce doctest.
"""

def average(num_list):
    """
    Return the average of a list of numbers.

      >>> average([1, 1, 1])
      1.0
      >>> average([1, 3])
      2.0
    """
    return sum(num_list) / len(num_list)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
