"""
NOTE: range(start, stop, step) generates values from start to stop-1,
      incrementing by step. 
"""
product = 1
numbers = range(5, 26, 5)

for number in numbers:
    product *= number

print(f"\nThe product of the values in {numbers} is {product}.")
