# Chapter 7 Doctest Quiz
#
# Write code to make each of the following doctests pass.


def sum_list(num_list):
    """
      >>> sum_list([1, 1, 1])
      3
      >>> sum_list([10, 1, 1])
      12
      >>> sum_list([10, 3, -1, 8])
      20
    """
    total = 0
    for num in num_list:
        total += num
    return total


def mult_list(num_list):
    """
      >>> mult_list([1, 1, 1])
      1
      >>> mult_list([10, 2, 3])
      60
      >>> mult_list([10, 3, -1, 8])
      -240
    """
    total = 1
    for num in num_list:
        total *= num
    return total


def factorial(n):
    """
      >>> factorial(5)
      120
      >>> factorial(6)
      720
    """
    total = 1
    for n in range(2, n+1):
        total *= n
    return total


if __name__ == "__main__":
    import doctest

    doctest.testmod()
