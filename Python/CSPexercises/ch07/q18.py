"""
NOTE:  Use this example to introduce doctest.
"""

def evens_product(to_num):
    """
    Return the product of even numbers between 2 and to_num.

      >>> evens_product(4)
      8
      >>> evens_product(6)
      48
    """
    product = 1
    for m in range(2, to_num+1, 2):
        product *= m
    return product 


if __name__ == "__main__":
    import doctest
    doctest.testmod()
