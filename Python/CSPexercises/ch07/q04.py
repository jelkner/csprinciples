"""
NOTE: Mention *additive identity* and *multiplicative identity*. This version
      can be used as solution to question 7 too.
"""

def mult_list(num_list):
    product = 1
    for n in num_list:
        product *= n
    return product 

my_nums = [3, 42, 87, 12, 9]
print(f"\nThe product of the numbers in {my_nums} is {mult_list(my_nums)}.")
