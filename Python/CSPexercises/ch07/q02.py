"""
NOTE: range(start, stop) generates values from start to stop-1. 
"""
sum = 0
things_to_add = range(1, 11)

for number in things_to_add:
    print(f"Adding {number} to sum...")
    sum += number

print(f"\nThe sum of the numbers from 1 to 10 is {sum}.")
