"""
Written by Jeff Elkner 2022-09-26.
Inspired by the work of Jake Stewart and Conrad Skelly.
See https://docs.python.org/3/library/operator.html for documentation on the
operator module.
"""
import operator
import random
import os

ops = {
    "+": (operator.add, "plus"),
    "-": (operator.sub, "minus"),
    "*": (operator.mul, "times"),
    "//": (operator.floordiv, "divided by"),
    "%": (operator.mod, "modulo"),
}
correct = 0

os.system("clear")
print("Welcome to the Math Quiz!\n")

numqs = input("How many questions do you want in your quiz? ")
while True:
    try:
        numqs = int(numqs)
        assert numqs > 0
        break
    except:
        os.system("clear")
        numqs = input("Please enter a positive number: ")

os.system("clear")
op = input("What kind of questions do you want (enter +, -, *, //, or %)? ")
while not op in ["+", "-", "*", "//", "%"]:
    os.system("clear")
    op = input("Please enter +, -, *, //, or % :")
os.system("clear")

for q in range(numqs):
    num1 = random.randint(1, 10)
    num2 = random.randint(1, 10)
    answer = ops[op][0](num1, num2)
    question = f"What is {num1} {ops[op][1]} {num2}? "
    reponse = int(input(question))

    if reponse == answer:
        print("That's right - well done.")
        correct += 1
    else:
        print(f"No, I'm afraid the answer is {answer}.")

print(f"I asked you {numqs} questions. You got {correct} of them right.\n")
