"""
A mini survey program originally written by Gabriel, Rockwell, and Ronan.

Refactored by Jeff.
"""
import time


def AskQuestion(question):
    while True:
        answer = input(question)
        answer = answer.upper()
        if len(answer) == 1 and answer in "ABCD":
            return answer
        print("Invalid answer. Choose A, B, C, or D.")


def ShowSelections(answers): 
    print("Here are your responses: ")
    time.sleep(1)
    for qnum, choice in enumerate(answers):
        print(f"For question number {qnum+1} you selected: {choice}")
        time.sleep(1)


print("""
This is the survey. You will be asked questions and your answers will recorded
and displayed as new information depending on what you selected at the end. You
are then asked to start. To start type yes (It doesn't need to be capitalized).
You will then go through the questions. When you are done, it will state that
you're done.  When you are done, your answers will be analyzed and displayed as
new information.
""")

choose_to_start = input("Start? ")

if choose_to_start.capitalize() == "Yes":
    f = open("questions.dat", "r")
    questions = f.readlines()
    f.close()
    answers = []
    for question in questions:
        answers.append(AskQuestion(question))
    print("Analyzing...")
    time.sleep(3)
    ShowSelections(answers)
else: 
    print("You chose not to start, if you want to start, run the program")
    print("again and type yes")
