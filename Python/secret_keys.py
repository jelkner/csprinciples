import random
import string 


def gen_key():
    key = random.choice(string.ascii_uppercase + string.digits)
    key += random.choice(string.ascii_uppercase + string.digits)
    return key 


def main():
    students = [
        "Gabriel",
        "Anupama",
        "Jack",
        "Kellan",
        "Alex",
        "Sean",
        "Noah",
        "Xander",
        "Emory",
        "Rockwell",
        "Blu",
        "Toby",
        "Mayah",
        "Evan",
        "Cristian",
        "Lary",
        "Conrad",
        "Kiersten",
        "Grant",
        "Jake",
        "Eimi",
        "Ronan"
    ]
    for student in students:
        print(f"{student:10s}{gen_key()}")


if __name__ == "__main__":
    main()
