# CS Principles

Resources for an introductory course in computer science principles.

The resources here were developed for an
[AP Computer Science Principles](https://en.wikipedia.org/wiki/AP_Computer_Science_Principles)
course, but with the explicit goal of making them generalizable outside that
context to be useful to anyone interested in teaching an introductory course
in computer science principles.


## CS Principles: Big Ideas in Programming

[CS Principles: Big Ideas in Programming](https://www.openbookproject.net/books/StudentCSP/index.html) is an online textbook developed by the CS Learning 4 U
group at Georgia Tech, which I am now in the process of remixing to update it
for the 2020 changes made to the AP CS Principles exam.

While the book uses Python's
[turtle](https://docs.python.org/3/library/turtle.html) as one of its four
"data types", the
[AP CSP pseudocode](https://apcentral.collegeboard.org/media/pdf/ap-computer-science-principles-exam-reference-sheet.pdf)
uses something more like
[Karl](https://en.wikipedia.org/wiki/Karel_(programming_language).

I plan to investigate some of the Karel implementations in Python, including:

* [stanford-karel](https://pypi.org/project/stanfordkarel/)
* [karel-the-robot](https://pypi.org/project/karel-the-robot/)

and see something similar could be added to
[Skulpt](https://skulpt.org/).
